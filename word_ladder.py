import re

def same(item, target):
  return len([c for (c, t) in zip(item, target) if c == t])
# Returns the letters that match with target word if they have the same length.

def build(pattern, words, seen, list):
  return [word for word in words
                 if re.search(pattern, word) and word not in seen.keys() and
                    word not in list]
# Searches words that have similar pattern to the chosen word. If the word in the search list hasn't been selected, it can be chosen to be the next word

def find(word, words, seen, target, path):
  # Iterating in each step until it reach the target word
  list = []
  for i in range(len(word)):
    # Add words that have similar pattern with the current word for each letter in word
    list += build(word[:i] + "." + word[i + 1:], words, seen, list)
  if len(list) == 0:
    return False
  # Sort list in alphabetically order
  list = sorted([(same(w, target), w) for w in list], reverse=True)
  for (match, item) in list:
    # If the word has the length equal the length of the target - 1, append it to the path
    if match >= len(target) - 1:
      if match == len(target) - 1:
        path.append(item)
      return True
    seen[item] = True
    #Start with the start word, append item to list, search for suitable items and if the path is not found remove the word.
  for (match, item) in list:
    path.append(item)
    if find(item, words, seen, target, path):
      return True
    path.pop()


def start_word(sword,words):
  #check if the start word is in the dictionary file or not
  if sword in words:
    return ""
  else:
    return "Start word is invalid"

def target_word(sword,tword,words):
  # check if the length of the target word is equal to the length of start word or not, if not print error
  if len(tword) == len(sword):
    #check if it's the same word as the start word or not, if not print error
    if tword != sword:
      # check if the start word is in the dictionary file or not, if not print error
      if tword in words:
        return ""
      else:
        return "Target word is invalid"
    else:
      return "Start word and target word cannot be the same"
  else:
    return "Start word's length and target word's length must be the same"

def unusable_words(unusewords,words):
  #for each word in the unusable word, if the word is in the dictionary file, remove it and return with the modified dictionary
  for word in unusewords:
    if word in words:
      words.remove(word)
      return words

def second_target(sword,tword,words):
  #if the start word is not in the dictionary file, the user need to re enter the start word
  if sword not in words:
    sword = input("Enter a different start word: ")
  s_error = start_word(sword, words)
  if s_error != "0":
    print(s_error)
  # if the target word is not in the dictionary file, the user need to re enter the target word
  if tword not in words:
    tword = input("Enter a different target word: ")
    target_word(sword, tword, words)
  path = [start]
  seen = {start: True}

  if find(sword, words, seen, tword, path):
    path.append(tword)
    print(len(path) - 1, path)
  else:
    print("No path found")

#enter the file name, if the file name is "dictionary.txt", open the file. If not print error and require user to reenter the file name
fname = input("Enter file name: ")
if fname == "dictionary.txt":
    file = open(fname)
else:
    print("file invalid, try again")
    fname = input("Enter file name: ")

lines = file.readlines()
unusablewords = list()
i = 0
errchk=False
while True:
  #user can start enter unusable words or press enter to move on to the next step
  while i == 0:
    uw = input("Enter unusable word or press enter to continue: ")
    if uw == "":
      i = 1
    else:
      unusablewords.append(uw)
  start = input("Enter start word: ").strip()
  sword = start

  words = []
  for line in lines:
    word = line.rstrip()
    if len(word) == len(start):
      words.append(word)
  unusable_words(unusablewords, words)
  if start not in words:
    start = input("Start word invalid. Try another word: ")
    for line in lines:
      word = line.rstrip()
      if len(word) == len(start):
        words.append(word)
  s_error = start_word(sword, words)
  if s_error == "0":
    break
  else:
    print(s_error)
    errchk = True

  target = input("Enter target word:").strip()
  if target not in words:
    target = input("Target word invalid. Try another word: ")
    target_word(sword, target, words)
  twordtwo = input("Enter second target word or else press enter to continue: ")
  if twordtwo == "":
    break
  else:
    second_target(target, twordtwo, words)
  break

count = 0
path = [start]
seen = {start : True}
if find(start, words, seen, target, path):
  path.append(target)
  print(len(path) - 1, path)
else:
  print("No path found")

